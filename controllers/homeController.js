let pathFile = "C:/Users/Caro d'amour/Desktop/Programmation 1/Projets/TP1-programmation1_BCormier-Gosselin/node-app/";
const axios = require('axios');
const path = require('path');

exports.redirectIndex = (request, response)=>{
    response.redirect("/search");
}

exports.sendIndex = (request, response)=>{
    response.render('search');
}

exports.responseSearch = (request, response)=>{
    let movieSearch = request.query.search;
    axios.get(`https://api.themoviedb.org/3/search/movie?api_key=19ad416db5f018cc8b2482a686deb0e4&query=${movieSearch}`)
    .then(result=>{
    let movies = result.data.results;
    if(movies.length === 0){
        response.send("Aucun résultat pour: " + movieSearch)
        };
    response.render("resultats", {movieRequested: movieSearch, moviesAvailable: movies});
    })   
    .catch(error=>{
        response.send('Votre demande na pas été exécutée!');
    });
};