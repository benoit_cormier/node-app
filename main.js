//modules
const express = require('express');
const server = express(); 
const path = require('path');
const http = require('http');
const expressLayouts = require('express-ejs-layouts');

//public
server.use(express.static(path.resolve(__dirname, "/public")));
server.set(express.static('public'))

//views
server.set('view engine', 'ejs')
server.set('views', __dirname+'/views');
server.use(express.static("views"))

//routes
const controller = require('./controllers/homeController');
server.get("/", controller.redirectIndex);
server.get("/search", controller.sendIndex);
server.get("/results", controller.responseSearch);

//middleware
const loggerMiddleware = function(request, response, next){
    console.log("request made");
    next();
}

server.use(express.json());

//définir le port
server.listen(3000, ()=>{
    console.log('server is 3000');
})
